package ru.t1.dkozyaikin.tm;

import ru.t1.dkozyaikin.tm.component.Boostrap;

public class Application {

    public static void main(String[] args) {
        Boostrap boostrap = new Boostrap();
        boostrap.run(args);
    }

}
