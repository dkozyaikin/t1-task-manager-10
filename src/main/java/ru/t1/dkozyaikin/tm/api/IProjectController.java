package ru.t1.dkozyaikin.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void displayProjects();

}
