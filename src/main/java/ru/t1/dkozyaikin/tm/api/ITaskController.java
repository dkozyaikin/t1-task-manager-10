package ru.t1.dkozyaikin.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void displayTasks();

}
