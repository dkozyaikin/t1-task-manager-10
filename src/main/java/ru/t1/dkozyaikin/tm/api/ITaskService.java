package ru.t1.dkozyaikin.tm.api;

import ru.t1.dkozyaikin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    void clear();

}
